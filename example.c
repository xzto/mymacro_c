#include "mymacro.c"

static const char *
cstr_or_indicate_null(const char *s) {
  return s ? s : "(null)";
}

static void
dostuff(MyMacro_Result *macros, const char *key) {
  printf("`%s` = `%s`\n", key, cstr_or_indicate_null(mymacro_find(macros->list, key)));
}

int main(int argc, char **argv) {
  if (argc != 2) {
    printf("expected 1 argument\n");
    return 1;
  }
  const char *filename = argv[1];
  MyMacro_Result macros;
  if (!mymacro_is_success(mymacro_from_filename(&macros, filename))) {
    printf("mymacro fail filename `%s`\n", filename);
    return 1;
  }

  MyMacro_List *m = macros.list;
  while (m) {
    printf("`%.*s` = `%.*s`\n",
           (int)m->key_len, m->key,
           (int)m->value_len, m->value);
    m = m->next;
  }
  // dostuff(&macros, "asdf");
  // dostuff(&macros, "zxcv");
  mymacro_result_deinit(&macros);
}
