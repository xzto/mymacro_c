#include "mymacrocomplex.c"

static const char *
cstr_or_indicate_null(const char *s) {
  return s ? s : "(null)";
}

static void
dostuff(MyMacro_Result *macros, MyMacro_Buf const *key) {
  MyMacro_Buf v;
  if (!mymacro_is_success(mymacro_find(macros->list, key, &v))) {
    v.ptr = 0;
    v.len = 0;
  }
  printf("`%.*s` = `%s`\n", (int)key->len, key->ptr, cstr_or_indicate_null(v.ptr));
}

int main(int argc, char **argv) {
  if (argc != 2) {
    printf("expected 1 argument\n");
    return 1;
  }
  const char *filename = argv[1];
  MyMacro_Result macros;
  if (!mymacro_is_success(mymacro_from_filename(&macros, filename))) {
    printf("mymacro fail filename `%s`\n", filename);
    return 1;
  }
  MyMacro_List *m = macros.list;
  while (m) {
    printf("`%.*s` = `%.*s`\n",
           (int)m->key.len, m->key.ptr,
           (int)m->value.len, m->value.ptr);
    m = m->next;
  }
  // dostuff(&macros, &mymacro_buf_from_cstr("asdf"));
  // dostuff(&macros, &mymacro_buf_from_cstr("zxcv"));
  mymacro_result_deinit(&macros);


  {
    size_t inc = 0;
    MyMacro_Arena arena;
    mymacro_arena_init(&arena, 4096);
    MyMacro_ArenaSave save_0;
    mymacro_arena_save_begin(&arena, &save_0);

    size_t size = 10;
    void *x = mymacro_arena_alloc_size_align(&arena, size, 1);
    MyMacro_ArenaSave save_a;
    mymacro_arena_save_begin(&arena, &save_a);
    MyMacro_ArenaSave save_b;
    mymacro_arena_save_begin(&arena, &save_b);

#if 0
    // This would crash
    inc = 10;
    x = mymacro_arena_realloc_size_align(&arena, x, size, size+inc, 1);
    size += inc;
#endif
    (void)mymacro_arena_alloc_size_align(&arena, 10000, 1);
    (void)mymacro_arena_alloc_size_align(&arena, 10000, 1);
    mymacro_arena_save_restore(&arena, &save_b);
    mymacro_arena_save_restore(&arena, &save_a);

    inc = 10000;
    x = mymacro_arena_realloc_size_align(&arena, x, size, size+inc, 1);
    size += inc;

    MyMacro_ArenaSave save_c;
    mymacro_arena_save_begin(&arena, &save_c);

    (void)mymacro_arena_alloc_size_align(&arena, 10000, 1);
    (void)mymacro_arena_alloc_size_align(&arena, 10000, 1);
#if 0
    // This would crash
    inc = 10000;
    x = mymacro_arena_realloc_size_align(&arena, x, size, size+inc, 1);
    size += inc;
#endif

    mymacro_arena_save_restore(&arena, &save_c);

    x = mymacro_arena_realloc_size_align(&arena, x, size, size+inc, 1);
    size += inc;

    // asm volatile ("int3");
    mymacro_arena_save_restore(&arena, &save_0);
    mymacro_arena_deinit(&arena);
  }
}
