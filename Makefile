CC ?= cc

.PHONY: all

all:
	$(CC) -Wall -Wextra -Werror -Wno-unused-function -Wconversion -Werror=type-limits example.c -ggdb3 -O0 -o example.out
	$(CC) -Wall -Wextra -Werror -Wno-unused-function -Wconversion -Werror=type-limits examplecomplex.c -ggdb3 -O0 -o examplecomplex.out
