/*
 * .macro file:
 * # comment
 * # whitespace at start and end of line is ignored
 * # define KEY to VALUE
 * KEY VALUE
 * # define KEY to empty string
 * KEY
 */

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>


#include "mymacro_common.c"


// This will be on an arena. Free the arena.
typedef struct MyMacro_List {
  // These strings are null terminated.
  char *key;
  size_t key_len;
  char *value;
  size_t value_len;
  struct MyMacro_List *next;
} MyMacro_List;

/// Return NULL if not found
MYMACRO_API char *
mymacro_find(MyMacro_List *macros, const char *key) {
  MyMacro_List *m = macros;
  while (m && strcmp(key, m->key) != 0) { m = m->next; }
  if (m) {
    mymacro_assert(m->value != 0);
    return m->value;
  }
  return 0;
}

/// The result from `mymacro_from_filename`. A linked list of macro definitions and an arena.
typedef struct {
  // Everything in the list is stored in the arena.
  MyMacro_List *list;
  MyMacro_Arena arena;
} MyMacro_Result;

/// Deinitialize a valid `MyMacro_Result` previously returned from `mymacro_from_filename`.
MYMACRO_API void
mymacro_result_deinit(MyMacro_Result *result) {
  mymacro_arena_deinit(&result->arena);
  memset(result, 0, sizeof(*result));
}

/// Parse a macro file. On success it has to be deinitialized with `mymacro_result_deinit`.
MYMACRO_API MyMacro_Err
mymacro_from_filename(MyMacro_Result *result, char const *filename) {
  FILE *stream = fopen(filename, "rb");
  if (!stream) {
    return MYMACRO_ERR;
  }
  MyMacro_Arena arena;
  mymacro_arena_init(&arena, 4096);
  MyMacro_List *macro_list = NULL;
  while (1) {
    char buf[2048];
    if (fgets(buf, sizeof(buf)/sizeof(buf[0]), stream) == 0) {
      break;
    }
    size_t line_len = strlen(buf);
    if (line_len > 0 && buf[line_len-1] == '\n') { buf[--line_len] = 0; }
    if (line_len > 0 && buf[line_len-1] == '\r') { buf[--line_len] = 0; }
    char *p = buf;
    // Skip whitespace at the start of the line
    while (*p == ' ' || *p == '\t') { p += 1; }
    // Skip comment
    if (*p == '#' || *p == 0) {
      continue;
    }
    // Get key
    const char *buf_key = p;
    size_t key_len = 0;
    const char *buf_value = "";
    size_t value_len = 0;
    // Skip while not whitespace
    while (*p && !(*p == ' ' || *p == '\t')) { p += 1; }
    // p is at end of key
    key_len = (uintptr_t)p - (uintptr_t)buf_key;
    if (*p == 0) {
      *p = 0;
      buf_value = "";
      value_len = 0;
    } else {
      *p = 0;
      p += 1;
      // Skip whitespace
      while (*p == ' ' || *p == '\t') { p += 1; }
      buf_value = p;
      // Get until end of line
      while (*p) { p += 1; }
      *p = 0;
      char *buf_value_end = p - 1;
      while (buf_value_end >= buf_value) {
        if (*buf_value_end == ' ' || *buf_value_end == '\t')  {
          buf_value_end -= 1;
          continue;
        } else {
          break;
        }
      }
      *(buf_value_end + 1) = 0;
      value_len = (uintptr_t)buf_value_end + 1 - (uintptr_t)buf_value;
    }
    // printf("mymacro: `%s` = `%s`\n", buf_key, buf_value);
    // add to the list
    {
      char *dup_value = mymacro_arena_dup_cstr_len(&arena, buf_value, value_len);
      // Search for this key and if I find it, remove the previous one.
      MyMacro_List **m = &macro_list;
      while (*m) {
        if ((key_len == (*m)->key_len) && (strncmp(buf_key, (*m)->key, key_len) == 0)) break;
        mymacro_assert(strncmp(buf_key, (*m)->key, key_len) != 0);
        m = &(*m)->next;
      }
      if (*m) {
        // Found previous key. Change the value and move it to the head.
        MyMacro_List *prev_m = *m;
        prev_m->value = dup_value;
        prev_m->value_len = value_len;
        (*m) = prev_m->next;
        prev_m->next = macro_list;
        macro_list = prev_m;
      } else {
        // Key did not exist. Add it to the list.
        MyMacro_List *newhead = mymacro_arena_alloc_type(&arena, MyMacro_List);
        char *dup_key = mymacro_arena_dup_cstr_len(&arena, buf_key, key_len);
        *newhead = (MyMacro_List){
          .key = dup_key,
          .key_len = key_len,
          .value = dup_value,
          .value_len = value_len,
          .next = macro_list,
        };
        macro_list = newhead;
      }
    }
  }
  fclose(stream);
  *result = (MyMacro_Result){
    .arena = arena,
    .list = macro_list,
  };
  return MYMACRO_SUCCESS;
}
