#ifndef mymacro_log
# define mymacro_log(...) fprintf(stderr, __VA_ARGS__)
#endif

#ifndef mymacro_abort
# define mymacro_abort() do { asm volatile("int3"); abort(); } while (0)
#endif

#ifndef mymacro_assert
# define mymacro_assert(x) do { if (!(x)) { mymacro_log("%s:%d: assertion failed: `%s`\n", __FILE__, __LINE__, #x); fflush(stdout); fflush(stderr); mymacro_abort(); } } while (0)
# define mymacro_assert_msg(x, msg) do { if (!(x)) { mymacro_log("%s:%d: assertion failed: `%s`: `%s`\n", __FILE__, __LINE__, #x, msg); fflush(stdout); fflush(stderr); mymacro_abort(); } } while (0)
#endif

#ifndef mymacro_unimplemented
# define mymacro_unimplemented() do { mymacro_log("%s:%d: mymacro_unimplemented()\n", __FILE__, __LINE__); fflush(stdout); fflush(stderr); mymacro_abort(); } while (0)
#endif

#ifndef MYMACRO_API
# define MYMACRO_API static
#endif

#ifndef MYMACRO_PAGE_SIZE
# define MYMACRO_PAGE_SIZE 4096
#endif

#define mymacro_array_count(x) (sizeof((x))/sizeof((x)[0]))
#define mymacro_min(a, b) ((a) < (b) ? (a) : (b))
#define mymacro_max(a, b) ((a) > (b) ? (a) : (b))

#ifndef MYMACRO_ARENA_SAFETY_CHECKS
# define MYMACRO_ARENA_SAFETY_CHECKS 1
#endif

/// An error code.
typedef struct {
  int x;
} MyMacro_Err;
#define MYMACRO_ERR ((MyMacro_Err){-1})
#define MYMACRO_SUCCESS ((MyMacro_Err){0})

/// Check if an error code is success.
MYMACRO_API int
mymacro_is_success(MyMacro_Err err) {
  return err.x == 0;
}

/// The memory owned by an arena block starts at the `MyMacro_ArenaBlock` struct.
typedef struct MyMacro_ArenaBlock {
  /// The size of this block in bytes, including `struct MyMacro_ArenaBlock`.
  size_t size;
  /// How much memory this block has already used.
  size_t idx;
  /// The previous block. Can be NULL.
  struct MyMacro_ArenaBlock *prev;
} MyMacro_ArenaBlock;

/// A linear allocator.
typedef struct {
  /// The first block of the arena.
  MyMacro_ArenaBlock *blocks;
#if MYMACRO_ARENA_SAFETY_CHECKS
  /// Each time `mymacro_arena_save_begin` is called this is incremented. Each
  /// time `mymacro_arena_save_restore` or `mymacro_arena_save_forget` gets
  /// called it checks if the value is right and decrements it. This catches
  /// restoring two saves in the incorrect order. It also verifies on
  /// `mymacro_arena_deinit` that the count is equal to zero.
  size_t saved_count;
  /// `saved_block_start` and `saved_block_idx` are used to catch a reallocation with memory that was created before the current save.
  /// When `saved_count > 0`, this is the start of the block (the block ptr) when `mymacro_arena_save_begin` was called.
  uintptr_t saved_block_start;
  /// When `saved_count > 0`, this is the idx of the block when the save happened.
  size_t saved_block_idx;
#endif
} MyMacro_Arena;

MYMACRO_API MyMacro_ArenaBlock *
mymacro_arena_block_create(size_t initial_size) {
  size_t default_block_size = MYMACRO_PAGE_SIZE;
  size_t this_block_size = mymacro_max(initial_size, default_block_size);
  mymacro_assert(this_block_size >= sizeof(MyMacro_ArenaBlock));
  MyMacro_ArenaBlock *block = malloc(this_block_size);
  mymacro_assert(block);
  *block = (MyMacro_ArenaBlock){
    .size = this_block_size,
    .idx = sizeof(MyMacro_ArenaBlock),
    .prev = NULL,
  };
  return block;
}

MYMACRO_API void
mymacro_arena_block_destroy(MyMacro_ArenaBlock *block) {
  free(block);
}

/// Initialize a `MyMacro_Arena*`. Crashes if OOM.
MYMACRO_API void
mymacro_arena_init(MyMacro_Arena *arena, size_t initial_size) {
  *arena = (MyMacro_Arena){
    .blocks = initial_size == 0 ? NULL : mymacro_arena_block_create(initial_size),
#if MYMACRO_ARENA_SAFETY_CHECKS
    .saved_count = 0,
    .saved_block_start = 0,
    .saved_block_idx = 0,
#endif
  };
}

/// Deinitialize a `MyMacro_Arena`.
MYMACRO_API void
mymacro_arena_deinit(MyMacro_Arena *arena) {
#if MYMACRO_ARENA_SAFETY_CHECKS
  mymacro_assert(arena->saved_count == 0);
#endif
  MyMacro_ArenaBlock *block = arena->blocks;
  while (block) {
    MyMacro_ArenaBlock *block_to_free = block;
    block = block_to_free->prev;
    mymacro_arena_block_destroy(block_to_free);
  }
  arena->blocks = NULL;
}


/// Allocate one `T`.
#define mymacro_arena_alloc_type(arena, T) (T*)mymacro_arena_alloc_size_align(arena, sizeof(T), 16)
/// Allocate an array of `T` of size `count`.
#define mymacro_arena_alloc_type_count(arena, T, count) (T*)mymacro_arena_alloc_size_align(arena, count*sizeof(T), 16)
// TODO: c99 does not have alignof.
// #define mymacro_arena_alloc_type_count(arena, T, count) (T*)mymacro_arena_alloc_size_align(arena, count*sizeof(T), alignof(T))
#define mymacro_arena_realloc_type_count(arena, T, old_ptr, old_count, new_count) (T*)mymacro_arena_realloc_size_align(arena, old_ptr, old_count*sizeof(T), new_count*sizeof(T), 16)
// #define mymacro_arena_realloc_type_count(arena, T, old_ptr, old_count, new_count) (T*)mymacro_arena_realloc_size_align(arena, old_ptr, old_count*sizeof(T), new_count*sizeof(T), alignof(T))

/// Allocate `size` bytes aligned to `align`. Crashes if OOM.
MYMACRO_API void *
mymacro_arena_alloc_size_align(MyMacro_Arena *arena, size_t size, size_t align) {
  size_t extra_size_for_align = align > 0 ? align - 1 : 0;
  size_t remaining_size = 0;
  if (arena->blocks) {
    mymacro_assert(arena->blocks->idx >= sizeof(MyMacro_ArenaBlock));
    mymacro_assert(arena->blocks->size >= arena->blocks->idx);
    remaining_size = arena->blocks->size - arena->blocks->idx;
  }
  // Allocate a new block if necessary.
  if (remaining_size < extra_size_for_align + size) {
    // Calculate the size for this block. The size will be rounded up to the next page boundary. The block size grows exponentially. It takes 4-6 generations for the block size to be multiplied by 10 (with individual allocations that are small compared to the block size).
    // Example with allocations of size 1 align 1:
    // gen 0: 4.00 KB       gen 1: 8.00 KB      gen 2: 16.00 KB     gen 3: 28.00 KB
    // gen 4: 44.00 KB      gen 5: 68.00 KB     gen 6: 104.00 KB    gen 7: 160.00 KB
    // gen 8: 244.00 KB     gen 9: 368.00 KB    gen 10: 556.00 KB   gen 11: 836.00 KB
    // If you have in the same arena a growing dynamic array and many small allocations, it is better to have a constant block size instead of growing it based on the previous size.

    size_t prev_arena_size = arena->blocks ? arena->blocks->size : 0;
    size_t actual_min_size = sizeof(MyMacro_ArenaBlock) + extra_size_for_align + size;
    size_t this_arena_size = actual_min_size + prev_arena_size;
    this_arena_size += this_arena_size / 2;
    size_t const page_size_minus_one = MYMACRO_PAGE_SIZE-1;
    this_arena_size = (this_arena_size + page_size_minus_one) & ~page_size_minus_one;
    mymacro_assert(this_arena_size > 0);
    MyMacro_ArenaBlock *prev_block = arena->blocks;
    arena->blocks = mymacro_arena_block_create(this_arena_size);
    arena->blocks->prev = prev_block;
  }
  MyMacro_ArenaBlock *block = arena->blocks;
  remaining_size = block->size - block->idx;
  mymacro_assert(remaining_size >= extra_size_for_align + size);
  uintptr_t result = (uintptr_t)block + block->idx;
  result = (result + extra_size_for_align) & ~(uintptr_t)extra_size_for_align;
  block->idx = (result + size - (uintptr_t)block);
  return (uint8_t*)result;
}

/// Reallocate memory at `old_ptr` of size `old_size` to `new_size` and `new_align`.
MYMACRO_API void *
mymacro_arena_realloc_size_align(MyMacro_Arena *arena, void *old_ptr, size_t old_size, size_t new_size, size_t new_align) {
  size_t const size_to_copy = mymacro_min(old_size, new_size);
  if (old_ptr != 0) {
    uintptr_t const old_start = (uintptr_t)old_ptr;
    uintptr_t const old_end = old_start + old_size;
#if MYMACRO_ARENA_SAFETY_CHECKS
    // Check that the old memory was not created before the current save state.
    {
      int is_valid = 0;
      MyMacro_ArenaBlock *b = arena->blocks;
      for (; b != (MyMacro_ArenaBlock*)arena->saved_block_start; b = b->prev) {
        // `b` can't possibly be null because `b->prev` must reach `arena->saved_block_start`. If saved_block_start was null, it stops the loop.
        mymacro_assert(b != NULL);
        // `b` is a valid block.
        uintptr_t b_start = (uintptr_t)b;
        uintptr_t b_idx = b_start + b->idx;
        if (old_start >= b_start && old_end <= b_idx) {
          // old memory is valid.
          is_valid = 1;
          break;
        }
      }
      if (!is_valid && b) {
        mymacro_assert(arena->saved_count > 0);
        mymacro_assert(b == (MyMacro_ArenaBlock*)arena->saved_block_start); // redundant assert
        uintptr_t b_start = (uintptr_t)b;
        uintptr_t b_good_start = b_start + arena->saved_block_idx;
        uintptr_t b_good_end = b_start + b->idx;
        mymacro_assert(b->idx >= arena->saved_block_idx);
        if (old_start >= b_good_start && old_end <= b_good_end) {
          // old memory is valid.
          is_valid = 1;
        }
      }
      if (!is_valid) {
        if (b != NULL) {
          mymacro_assert_msg(0, "you reallocated memory that was created before the current save");
        } else mymacro_assert_msg(0, "the old memory was not from this arena");
      }
    }
#endif
    size_t new_align_minus_one = new_align > 0 ? new_align - 1 : 0;
    // Check if the old memory is aligned to `new_align`.
    if (((old_start + new_align_minus_one) & ~(uintptr_t)new_align_minus_one) != old_start) {
      // Can't use the same pointer.
      void *result = mymacro_arena_alloc_size_align(arena, new_size, new_align);
      memcpy(result, old_ptr, size_to_copy);
      return result;
    }
    uintptr_t new_end = old_start + new_size;
    uintptr_t astart = (uintptr_t)(arena->blocks);
    uintptr_t aidx = astart + arena->blocks->idx;
    uintptr_t afinalend = astart + arena->blocks->size;
    // Check if the old memory ends at the current arena state and it fits in the current arena block. This works for both increasing and decreasing reallocations.
    if (old_end == aidx && new_end <= afinalend) {
      arena->blocks->idx = new_end - astart;
      return old_ptr;
    }
  } else {
    mymacro_assert(old_size == 0);
  }
  // Can't use the same pointer.
  void *result = mymacro_arena_alloc_size_align(arena, new_size, new_align);
  memcpy(result, old_ptr, size_to_copy);
  return result;
}

/// Duplicate a string at `buf` of size `len` and null terminate it. Crashes if OOM.
MYMACRO_API char *
mymacro_arena_dup_cstr_len(MyMacro_Arena *arena, const char *buf, size_t len) {
  char *result = mymacro_arena_alloc_size_align(arena, len+1, 1);
  memcpy(result, buf, len);
  result[len] = 0;
  return result;
}

/// Duplicate a string at `buf` of size `strlen(buf)` and null terminate it. Crashes if OOM.
MYMACRO_API char *
mymacro_arena_dup_cstr(MyMacro_Arena *arena, const char *buf) {
  return mymacro_arena_dup_cstr_len(arena, buf, strlen(buf));
}

// NOTE: I did not test MyMacro_ArenaSave too much.
/// A saved state of a `MyMacro_Arena`.
typedef struct {
  MyMacro_ArenaBlock *block;
#if MYMACRO_ARENA_SAFETY_CHECKS
  size_t prev_saved_count;
  uintptr_t prev_saved_block_start;
  size_t prev_saved_block_idx;
#endif
  size_t idx;
} MyMacro_ArenaSave;

/// Save a state of a `MyMacro_Arena`. Restore it with `mymacro_arena_save_restore` or forget it with `mymacro_arena_save_forget`.
MYMACRO_API void
mymacro_arena_save_begin(MyMacro_Arena *arena, MyMacro_ArenaSave *ret_saved) {
  *ret_saved = (MyMacro_ArenaSave){
    .block = arena->blocks,
#if MYMACRO_ARENA_SAFETY_CHECKS
    .prev_saved_count = arena->saved_count++,
    .prev_saved_block_start = arena->saved_block_start,
    .prev_saved_block_idx = arena->saved_block_idx,
#endif
    .idx = arena->blocks ? arena->blocks->idx : 0,
  };
#if MYMACRO_ARENA_SAFETY_CHECKS
  arena->saved_block_start = (uintptr_t)arena->blocks;
  arena->saved_block_idx = ret_saved->idx;
#endif
}

/// Forget about a saved arena state. This is needed if you need to restore an arena to a state on error and on success you keep it as is.
MYMACRO_API void
mymacro_arena_save_forget(MyMacro_Arena *arena, MyMacro_ArenaSave *saved) {
  (void)arena;
#if MYMACRO_ARENA_SAFETY_CHECKS
  mymacro_assert(saved->prev_saved_count == --arena->saved_count);
  arena->saved_block_start  = saved->prev_saved_block_start;
  arena->saved_block_idx    = saved->prev_saved_block_idx;
#endif
  *saved = (MyMacro_ArenaSave){ .block = (MyMacro_ArenaBlock*)((uintptr_t)(-1)), .idx = 0 };
}

/// Restore a state of `MyMacro_ArenaSave` that was saved with `mymacro_arena_save_begin`.
MYMACRO_API void
mymacro_arena_save_restore(MyMacro_Arena *arena, MyMacro_ArenaSave *saved_) {
#if MYMACRO_ARENA_SAFETY_CHECKS
  mymacro_assert(saved_->prev_saved_count == --arena->saved_count);
  arena->saved_block_start  = saved_->prev_saved_block_start;
  arena->saved_block_idx    = saved_->prev_saved_block_idx;
#endif
  // NOTE: Don't reallocate something that is born before or that lives longer than the `MyMacro_ArenaSave`.
  // The first block will not be freed. It will stay as the head of the block list. I will only free the blocks between saved->block and this first_block (not inclusive).
  MyMacro_ArenaBlock *first_block = arena->blocks;
  // Copy `saved_` into the stack and write garbage to the old value. Write garbage to prevent restoring twice. Make a copy in the stack because `saved_` might be inside a block that will be freed.
  MyMacro_ArenaSave saved = *saved_;
  *saved_ = (MyMacro_ArenaSave){ .block = (MyMacro_ArenaBlock*)((uintptr_t)(-1)), .idx = 0 };
  while (1) {
    if (arena->blocks == NULL) {
      // If this assert crashes, you called restore with wrong arguments (saved does not match arena or you tried to restore to a newer state).
      mymacro_assert(saved.block == NULL);
      break;
    }
    if (arena->blocks == saved.block) {
      // Assert that the saved position is older than the current position. See the NOTE regarding reallocation.
      mymacro_assert((uintptr_t)saved.block + saved.idx <= (uintptr_t)arena->blocks + arena->blocks->idx);
      arena->blocks->idx = saved.idx;
      break;
    }
    MyMacro_ArenaBlock *block_to_free = arena->blocks;
    arena->blocks = block_to_free->prev;
    // Don't free the first block. I'll insert it to the list later.
    if (block_to_free != first_block) {
      mymacro_arena_block_destroy(block_to_free);
    } else {
      // Make sure I can't access this value because it might be garbage later.
      first_block->prev = NULL;
    }
  }
  mymacro_assert(saved.block == arena->blocks);
  if (first_block != arena->blocks) {
    // Insert `first_block` to the list of blocks. This is done to prevent reallocating blocks too often if I save and restore in a loop.
    mymacro_assert(first_block->size >= sizeof(MyMacro_ArenaBlock));
    first_block->idx = sizeof(MyMacro_ArenaBlock);
    first_block->prev = arena->blocks;
    arena->blocks = first_block;
  }
}

#if 0
{
  // NOTE: Don't reallocate something that is born before or that lives longer than the `MyMacro_ArenaSave` during that saved state.
  uint64_t *x = mymacro_arena_alloc_type_count(arena, uint64_t, 20);
  x = mymacro_arena_realloc_type_count(arena, uint64_t, x, 20, 23); // this is fine
  {
    MyMacro_ArenaSave saved;
    mymacro_arena_save_begin(arena, &saved);
    // Can not reallocate a pointer that was allocated before the save.
    // x is wrong.
    x = mymacro_arena_realloc_type_count(arena, uint64_t, x, 23, 40);
    x = mymacro_arena_realloc_type_count(arena, uint64_t, x, 40, 5);

    // Reallocating something that was created after the save is fine, however.
    // y is fine.
    uint64_t *y = mymacro_arena_alloc_type_count(arena, uint64_t, 6);
    y = mymacro_arena_realloc_type_count(arena, uint64_t, y, 6, 123);
    y = mymacro_arena_realloc_type_count(arena, uint64_t, y, 123, 1);

    // When restore is called, it might or might not crash because of `x`.
    mymacro_arena_restore(arena, &saved);
    // y is invalid now
  }

  // I could reallocate x here, but not inside the temporary save.
  x = mymacro_arena_realloc_type_count(arena, uint64_t, x, 23, 5); // this is fine

  // Using a pointer after a save is forgotten is fine.
  uint64_t *z;
  {
    MyMacro_ArenaSave saved;
    mymacro_arena_save_begin(arena, &saved);
    z = mymacro_arena_alloc_type_count(arena, uint64_t, 7);
    mymacro_arena_save_forget(arena, &saved);
  }
  // z is still valid
}
#endif
