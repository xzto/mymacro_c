#if 0
//Usage:
{
  MyMacro_Result macros;
  if (mymacro_is_success(mymacro_from_filename(&macros, filename))) {
    success;
  } else {
    error;
  }
}


#endif
/*
.macro file:
maps keys (string) to values (string)
# comment
# whitespace (tab/space) is trimmed
# define KEY to VALUE
define KEY VALUE
# define KEY to empty string
define KEY

# include file - the filename can not start or end with whitespace
include /path/to/file.macro
include relative/to/this/file/file.macro


# reference a previously defined key
define prev qwerty
define new %{prev}uiop
# new is qwertyuiop

# reference syntax: %{KEY}
# the thing inside {} will be whitespace trimmed.
# if the key does not exist it is an error

# escape rules for %{}:
evaluation
%  -> %
%X -> %X for any byte X except "{" and "%"
%% -> %
%{A} -> expand A
%%{ -> %{
%%%{A} -> %expand A
%%%%{ -> %%{

---

if you redefine a key, it will be overwritten
define X 10
define X 1234
# X is 1234

*/


#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

// #define malloc(x) ({ mymacro_log("%s:%d: allocating %lu\n", __FILE__, __LINE__, x); malloc(x); })
// #define free(x) ({ mymacro_log("%s:%d: free %p\n", __FILE__, __LINE__, x); free(x); })

// TODO: Put the common stuff in mymacro_utils.c
#include "mymacro_common.c"

/// A pointer and size to a region of memory.
typedef struct {
  char *ptr;
  size_t len;
} MyMacro_Buf;

/// Get a `MyMacro_Buf` from a string literal.
#define mymacro_buf_from_literal(cstr) ((MyMacro_Buf){.ptr = (cstr), .len = mymacro_array_count(cstr) - 1})
/// Get a `MyMacro_Buf` from a null terminated string.
#define mymacro_buf_from_cstr(cstr) ((MyMacro_Buf){.ptr = (cstr), .len = strlen(cstr)})

/// Check if two `MyMacro_Buf` are equal.
MYMACRO_API int
mymacro_buf_eq(MyMacro_Buf const *a, MyMacro_Buf const *b) {
  if (a->len != b->len) return 0;
  return memcmp(a->ptr, b->ptr, a->len) == 0;
}

/// Make a new `MyMacro_Buf` that starts at index `lo` and has a length fo `hi - lo`.
MYMACRO_API void
mymacro_buf_slice(MyMacro_Buf const *buf, size_t lo, size_t hi, MyMacro_Buf *result) {
  // buf may alias with result.
  mymacro_assert(lo <= hi && lo <= buf->len && hi <= buf->len);
  *result = (MyMacro_Buf){
    .ptr = buf->ptr + lo,
    .len = hi - lo,
  };
}

/// Trims whitespace from the left in `buf`.
MYMACRO_API void
mymacro_buf_trim_whitespace_left(MyMacro_Buf *buf) {
  int stop = 0;
  size_t i = 0;
  for (; i < buf->len && !stop; i++) {
    switch (buf->ptr[i]) {
    case ' ': case '\t': case '\r': case '\n': {
      continue;
    } break;
    default: {
      stop = 1;
    } break;
    }
  }
  mymacro_buf_slice(buf, i-(size_t)(stop?1:0), buf->len, buf);
}

/// Trims whitespace from the right in `buf`.
MYMACRO_API void
mymacro_buf_trim_whitespace_right(MyMacro_Buf *buf) {
  int stop = 0;
  size_t j = buf->len;
  for (; j > 0 && !stop;) {
    j--;
    switch (buf->ptr[j]) {
    case ' ': case '\t': case '\r': case '\n': {
      continue;
    } break;
    default: {
      stop = 1;
    } break;
    }
  }
  mymacro_buf_slice(buf, 0, j+(size_t)(stop?1:0), buf);
}

/// Trims whitespace from the left and right in `buf`.
MYMACRO_API void
mymacro_buf_trim_whitespace(MyMacro_Buf *buf) {
  mymacro_buf_trim_whitespace_left(buf);
  mymacro_buf_trim_whitespace_right(buf);
}

/// Finds the index from the left of `to_find_scalar`.
MYMACRO_API MyMacro_Err
mymacro_buf_index_of_scalar(MyMacro_Buf const *buf, char to_find_scalar, size_t *ret_idx) {
  for (size_t i = 0; i < buf->len; i++) {
    if (buf->ptr[i] == to_find_scalar) {
      *ret_idx = i;
      return MYMACRO_SUCCESS;
    }
  }
  return MYMACRO_ERR;
}

/// Finds the index from the left of any element of `to_find_any`.
MYMACRO_API MyMacro_Err
mymacro_buf_index_of_any(MyMacro_Buf const *buf, MyMacro_Buf const *to_find_any, size_t *ret_idx) {
  for (size_t i = 0; i < buf->len; i++) {
    for (size_t j = 0; j < to_find_any->len; j++) {
      char const to_find_scalar = to_find_any->ptr[j];
      if (buf->ptr[i] == to_find_scalar) {
        *ret_idx = i;
        return MYMACRO_SUCCESS;
      }
    }
  }
  return MYMACRO_ERR;
}

/// Trims `buf` from the left, finds a "keyword" (no whitespace) and advances `buf`.
MYMACRO_API MyMacro_Err
mymacro_buf_trim_and_chop_by_whitespace(MyMacro_Buf *buf, MyMacro_Buf *ret_keyword) {
  mymacro_buf_trim_whitespace_left(buf);
  if (buf->len <= 0) return MYMACRO_ERR;
  size_t space_idx;
  if (mymacro_is_success(mymacro_buf_index_of_any(buf, &mymacro_buf_from_literal(" \t\r\n"), &space_idx))) {
    mymacro_assert(space_idx > 0);
    mymacro_buf_slice(buf, 0, space_idx, ret_keyword);
    mymacro_buf_slice(buf, space_idx, buf->len, buf);
    return MYMACRO_SUCCESS;
  }
  *ret_keyword = *buf;
  mymacro_buf_slice(buf, buf->len, buf->len, buf);
  return MYMACRO_SUCCESS;
}

/// Gets dirname and basename from `full` and st ores it in `ret_dir` and `ret_base`.
MYMACRO_API void
mymacro_buf_filename_get_dirname_and_basename(MyMacro_Buf const *full, MyMacro_Buf *ret_dir, MyMacro_Buf *ret_base) {
  // TODO: Write unit tests for this function.
  // "a" => "", "a"
  // "/a" => "/", "a"
  // "/a//b" => "/a//", "b"
  // "/a//b/" => "/a//", "b/"
  if (full->len > 0) {
    int found = 0;
    int only_got_slashes = full->ptr[full->len] == '/';
    size_t i = full->len;
    while (i > 0) {
      i--;
      if (full->ptr[i] != '/') {
        only_got_slashes = 0;
      } else if (!only_got_slashes) {
        found = 1;
        break;
      }
    }
    if (only_got_slashes) {
      mymacro_assert(!found);
      i = full->len-1;
    }
    mymacro_buf_slice(full, 0, i+(found?1:0), ret_dir);
    mymacro_buf_slice(full, i+(found?1:0), full->len, ret_base);
  } else {
    *ret_dir = *ret_base = *full;
  }

  if (ret_base->len == 0) { mymacro_assert(ret_dir->len == 0); }
  if (ret_dir->len > 0) { mymacro_assert(ret_dir->ptr[ret_dir->len-1] == '/'); }
  if (ret_base->len > 0) { mymacro_assert(ret_base->ptr[0] != '/'); }
}

/// A linked list of macro definitions. The contents are on `MyMacro_Result`'s arena.
typedef struct MyMacro_List {
  // These strings are null terminated.
  MyMacro_Buf key;
  MyMacro_Buf value;
  struct MyMacro_List *next;
} MyMacro_List;

/// Find a value of key `key` in `macros`. Return NULL if not found
MYMACRO_API MyMacro_Err
mymacro_find(MyMacro_List const *macros, MyMacro_Buf const *key, MyMacro_Buf *ret_val) {
  MyMacro_List const *m = macros;
  while (m && !mymacro_buf_eq(key, &m->key)) { m = m->next; }
  if (m) {
    mymacro_assert(m->value.ptr != 0);
    *ret_val = m->value;
    return MYMACRO_SUCCESS;
  }
  return MYMACRO_ERR;
}


/// Parse the value `value_unprocessed` with `macro_list` and store it in `ret_value` (allocates memory in `arena` on success).
MYMACRO_API MyMacro_Err
mymacro_parse_value(MyMacro_Arena *arena, MyMacro_List **macro_list, MyMacro_Buf const *value_unprocessed, MyMacro_Buf *ret_value) {
  // TODO Maybe I should make a new error enum for this function.

  // `saved` will be restored on failure and will be forgotten on success.
  MyMacro_ArenaSave saved;
  mymacro_arena_save_begin(arena, &saved);

  // A dynamic array.
  MyMacro_Buf buf = (MyMacro_Buf){.ptr = 0, .len = 0};
  size_t buf_capacity = 0;

#define buf_require_unused_capacity(x) do {                                                        \
  if (buf_capacity - buf.len < (x)) {                                                              \
    size_t new_buf_capacity = 1; /*(buf_capacity+1)*2;*/                                           \
    size_t minimum_new_buf_capacity = buf.len + (x);                                               \
    new_buf_capacity = mymacro_max(new_buf_capacity, minimum_new_buf_capacity);                    \
    buf.ptr = mymacro_arena_realloc_size_align(arena, buf.ptr, buf_capacity, new_buf_capacity, 1); \
    buf_capacity = new_buf_capacity;                                                               \
  }                                                                                                \
} while(0)

#define buf_copy_from_ptr_len(from_ptr, from_len) do { \
  buf_require_unused_capacity((from_len));             \
  memcpy(buf.ptr+buf.len, (from_ptr), (from_len));     \
  buf.len += (from_len);                               \
  mymacro_assert(buf.len <= buf_capacity);             \
} while (0)

  {
    ssize_t None = -123;
    ssize_t normal_start_idx = 0;
    ssize_t percent_idx = None;
    ssize_t i;
    for (i = 0; i < (ssize_t)value_unprocessed->len; i++) {
      char ch = value_unprocessed->ptr[i];
      if (percent_idx == None) {
        mymacro_assert(normal_start_idx != None);
        if (ch == '%') {
          buf_copy_from_ptr_len(&value_unprocessed->ptr[normal_start_idx], (size_t)(i - normal_start_idx));
          percent_idx = i;
          normal_start_idx = None;
        }
      } else {
        mymacro_assert(normal_start_idx == None);
        if (percent_idx == i - 1) {
          if (ch == '%') {
            normal_start_idx = i;
            percent_idx = None;
          } else if (ch != '{') {
            normal_start_idx = percent_idx;
            percent_idx = None;
          }
        } else {
          mymacro_assert(value_unprocessed->ptr[percent_idx + 1] == '{');
          if (ch == '}') {
            MyMacro_Buf referenced_key;
            mymacro_buf_slice(value_unprocessed, (size_t)percent_idx + 2, (size_t)i, &referenced_key);
            mymacro_buf_trim_whitespace(&referenced_key);
            MyMacro_Buf referenced_val;
            if (!mymacro_is_success(mymacro_find(*macro_list, &referenced_key, &referenced_val))) {
              mymacro_arena_save_restore(arena, &saved);
              return MYMACRO_ERR;
            }
            buf_copy_from_ptr_len(referenced_val.ptr, referenced_val.len);
            percent_idx = None;
            normal_start_idx = i + 1;
          }
        }
      }
    }
    if (percent_idx == None) {
      mymacro_assert(normal_start_idx != None);
      buf_copy_from_ptr_len(&value_unprocessed->ptr[normal_start_idx], (size_t)(i - normal_start_idx));
      percent_idx = None;
      normal_start_idx = None;
    } else {
      mymacro_assert(normal_start_idx == None);
      mymacro_assert(value_unprocessed->ptr[percent_idx+1] == '{');
      // Error: invalid reference syntax: missing '}'.
      mymacro_arena_save_restore(arena, &saved);
      return MYMACRO_ERR;
    }
    buf_require_unused_capacity(1);
    buf.ptr[buf.len] = 0;

    mymacro_arena_save_forget(arena, &saved);

    *ret_value = buf;
    return MYMACRO_SUCCESS;
  }
#undef buf_copy_from_ptr_len
#undef buf_require_unused_capacity
}

/// Parse the value and add to the list.
static MyMacro_Err
mymacro__parse_value_and_add(MyMacro_Arena *arena, MyMacro_List **macro_list, MyMacro_Buf const *key, MyMacro_Buf const *value_unprocessed) {
  // mymacro_log("mymacro: `%.*s` = `%.*s`\n", (int)key->len, key->ptr, (int)value_unprocessed->len, value_unprocessed->ptr);
  MyMacro_Buf new_value;
  if (!mymacro_is_success(mymacro_parse_value(arena, macro_list, value_unprocessed, &new_value))) {
    return MYMACRO_ERR;
  }
  {
    // Search for this key and if I find it, remove the previous one.
    MyMacro_List **m = macro_list;
    while (*m && !mymacro_buf_eq(key, &(*m)->key)) {
      m = &(*m)->next;
    }
    if (*m) {
      // Found previous key. Change the value and move it to the head.
      MyMacro_List *prev_m = *m;
      prev_m->value = new_value;
      (*m) = prev_m->next;
      prev_m->next = *macro_list;
      *macro_list = prev_m;
      return MYMACRO_SUCCESS;
    }
  }
  // Key did not exist. Add it to the list.
  MyMacro_List *newhead = mymacro_arena_alloc_type(arena, MyMacro_List);
  char *dup_key = mymacro_arena_dup_cstr_len(arena, key->ptr, key->len);
  *newhead = (MyMacro_List){
    .key = (MyMacro_Buf){ .ptr = dup_key, .len = key->len },
    .value = new_value,
    .next = *macro_list,
  };
  *macro_list = newhead;
  return MYMACRO_SUCCESS;
}

/// A linked list of open files.
typedef struct MyMacro__FileList {
  FILE *stream;
  MyMacro_Buf filename; // nullter // on arena
  MyMacro_Buf dirname; // not nullter // substring of filename
  MyMacro_Buf basename; // nullter // substring of filename
  MyMacro_ArenaSave stack_arena_saved;
  int line1; // updated every time get_line is called
  struct MyMacro__FileList *next; // can be null
} MyMacro__FileList;

/// A file stack.
typedef struct {
  MyMacro_Arena arena;
  MyMacro__FileList *list; // on arena
  int depth;
} MyMacro__FileStack;

/// Initialize a `MyMacro__FileStack`. Deinitialize it with `mymacro__file_stack_deinit`.
static void
mymacro__file_stack_init(MyMacro__FileStack *file_stack) {
  *file_stack = (MyMacro__FileStack){ .arena = {0}, .list = NULL };
  mymacro_arena_init(&file_stack->arena, 4096);
}

/// Close one file from the bottom of the stack.
static void
mymacro__file_stack_close_one_file(MyMacro__FileStack *file_stack) {
  mymacro_assert(file_stack->list && file_stack->depth > 0);
  fclose(file_stack->list->stream);
  MyMacro_ArenaSave saved = file_stack->list->stack_arena_saved;
  // `saved` will free the current head's memory.
  file_stack->list = file_stack->list->next;
  file_stack->depth -= 1;
  mymacro_arena_save_restore(&file_stack->arena, &saved);
}

/// Deinitiailze a `MyMacro__FileStack`.
static void
mymacro__file_stack_deinit(MyMacro__FileStack *file_stack) {
  while (file_stack->list) {
    mymacro__file_stack_close_one_file(file_stack);
  }
  mymacro_assert(file_stack->list == NULL && file_stack->depth == 0);
  mymacro_arena_deinit(&file_stack->arena);
  memset(file_stack, 0, sizeof(*file_stack));
}

/// Error code from `mymacro__file_stack_add_file`.
typedef enum {
  MyMacro__FileStackAddFileErr_Success,
  MyMacro__FileStackAddFileErr_MaxIncludeDepth,
  MyMacro__FileStackAddFileErr_Filesystem,
} MyMacro__FileStackAddFileErr;

/// The maximum file stack depth.
#define MYMACRO_FILE_STACK_DEPTH_MAX 100
/// Add a new file to the stack.
static MyMacro__FileStackAddFileErr
mymacro__file_stack_add_file(MyMacro__FileStack *file_stack, char const *filename) {
  size_t const filename_len = strlen(filename);
  if (filename_len <= 0) {
    return MyMacro__FileStackAddFileErr_Filesystem;
  }
  if (file_stack->depth >= MYMACRO_FILE_STACK_DEPTH_MAX) {
    return MyMacro__FileStackAddFileErr_MaxIncludeDepth;
  }
  {
    // TODO: Verify this code is 100% correct.
    // `stack_arena_saved` will be restored on failure and will be moved to the file stack on success.
    MyMacro_ArenaSave stack_arena_saved;
    mymacro_arena_save_begin(&file_stack->arena, &stack_arena_saved);
    // filename_buf, dirname and basename will later be copied into the
    // arena and their values might change (dirname might change).
    MyMacro_Buf filename_buf = (MyMacro_Buf){
      .ptr = (char*)filename, // This const cast is ok.
      .len = filename_len,
    };
    MyMacro_Buf dirname, basename;
    mymacro_buf_filename_get_dirname_and_basename(&filename_buf, &dirname, &basename);
    if (basename.len <= 0) {
      // Error: filename was probably '/'
      mymacro_arena_save_restore(&file_stack->arena, &stack_arena_saved);
      return MyMacro__FileStackAddFileErr_Filesystem;
    }
    if (dirname.len > 0 && dirname.ptr[0] == '/') {
      // full path, leave it as is
      filename_buf = (MyMacro_Buf){
        .ptr = mymacro_arena_dup_cstr_len(&file_stack->arena, filename, filename_len),
        .len = filename_len,
      };
      mymacro_buf_slice(&filename_buf, 0, dirname.len, &dirname);
      mymacro_buf_slice(&filename_buf, dirname.len, filename_buf.len, &basename);
      if (basename.len > 0) { mymacro_assert(basename.ptr[0] != '/'); }
    } else {
      // path is relative to the current file
      MyMacro_Buf const prev_dirname = (file_stack->list != NULL) ? file_stack->list->dirname : mymacro_buf_from_literal("./");
      size_t const newdirname_len = prev_dirname.len + dirname.len;
      char *buf = mymacro_arena_alloc_size_align(&file_stack->arena, newdirname_len + basename.len + 1, 1);
      size_t i = 0;
      memcpy(buf+i, prev_dirname.ptr, prev_dirname.len);
      i += prev_dirname.len;

      memcpy(buf+i, dirname.ptr, dirname.len);
      i += dirname.len;

      memcpy(buf+i, basename.ptr, basename.len);
      i += basename.len;

      buf[i] = 0;

      filename_buf = (MyMacro_Buf){
        .ptr = buf,
        .len = i,
      };
      mymacro_buf_slice(&filename_buf, 0, newdirname_len, &dirname);
      mymacro_buf_slice(&filename_buf, newdirname_len, filename_buf.len, &basename);
      if (dirname.len > 0) { mymacro_assert(dirname.ptr[dirname.len-1] == '/'); }
      if (basename.len > 0) { mymacro_assert(basename.ptr[0] != '/'); }
    }
    // filename_buf, dirname and basename have their final values here.
    mymacro_assert(filename_buf.ptr[filename_buf.len] == 0);
    FILE *stream = fopen(filename_buf.ptr, "rb");
    if (!stream) {
      mymacro_arena_save_restore(&file_stack->arena, &stack_arena_saved);
      return MyMacro__FileStackAddFileErr_Filesystem;
    }
    MyMacro__FileList *file = mymacro_arena_alloc_type(&file_stack->arena, MyMacro__FileList);
    *file = (MyMacro__FileList){
      .stream = stream,
      .filename = filename_buf,
      .dirname = dirname,
      .basename = basename,
      .stack_arena_saved = stack_arena_saved,
      .line1 = 0,
      .next = file_stack->list,
    };
    file_stack->list = file;
    file_stack->depth += 1;
    return MyMacro__FileStackAddFileErr_Success;
  }
}

/// Get a new line from the file stack.
static MyMacro_Err
mymacro__file_stack_get_line(MyMacro__FileStack *file_stack, char *buf, size_t buf_size) {
  while (1) {
    if (file_stack->depth <= 0) break;
    mymacro_assert(file_stack->list != NULL && file_stack->list->stream != NULL);
    if (fgets(buf, (int)buf_size, file_stack->list->stream) == 0) {
      // File ended
      mymacro__file_stack_close_one_file(file_stack);
      continue;
    } else {
      file_stack->list->line1 += 1;
      return MYMACRO_SUCCESS;
    }
  }
  return MYMACRO_ERR;
}


/// The result from `mymacro_from_filename`. A linked list of macro definitions and an arena.
typedef struct {
  // Everything in the list is stored in the arena.
  MyMacro_List *list;
  MyMacro_Arena arena;
} MyMacro_Result;

/// Deinitialize a valid `MyMacro_Result` previously returned from `mymacro_from_filename`.
MYMACRO_API void
mymacro_result_deinit(MyMacro_Result *result) {
  mymacro_arena_deinit(&result->arena);
  memset(result, 0, sizeof(*result));
}


/// Parse a macro file. On success it has to be deinitialized with `mymacro_result_deinit`.
MYMACRO_API MyMacro_Err
mymacro_from_filename(MyMacro_Result *result, char const *filename) {
  // Final arena
  MyMacro_Arena arena;
  mymacro_arena_init(&arena, 4096);

  MyMacro__FileStack file_stack;
  mymacro__file_stack_init(&file_stack);
  // defer mymacro__file_stack_deinit(&file_stack);
  switch (mymacro__file_stack_add_file(&file_stack, filename)) {
  case MyMacro__FileStackAddFileErr_Success: {
    // ok
  } break;
  default: {
    // Error
    mymacro_log("mymacro: failed to open first file `%s`\n", filename);
    mymacro__file_stack_deinit(&file_stack);
    mymacro_arena_deinit(&arena);
    return MYMACRO_ERR;
  } break;
  }

  MyMacro_List *macro_list = NULL;
  int error_count = 0;
  while (error_count == 0) {
    char buf[2048];
    if (!mymacro_is_success(mymacro__file_stack_get_line(&file_stack, buf, mymacro_array_count(buf)))) {
      break;
    }
    size_t line_len = strlen(buf);
    if (line_len > 0 && buf[line_len-1] == '\n') { buf[--line_len] = 0; }
    if (line_len > 0 && buf[line_len-1] == '\r') { buf[--line_len] = 0; }
    MyMacro_Buf line = (MyMacro_Buf){.ptr = buf, .len = line_len};
    mymacro_buf_trim_whitespace(&line);

    // Skip empty lines and comments.
    if (line.len <= 0 || line.ptr[0] == '#') {
      continue;
    }

    MyMacro_Buf directive;
    if (mymacro_is_success(mymacro_buf_trim_and_chop_by_whitespace(&line, &directive))) {
      MyMacro_Buf const directive_str_define = mymacro_buf_from_literal("define");
      MyMacro_Buf const directive_str_include = mymacro_buf_from_literal("include");
      if (mymacro_buf_eq(&directive, &directive_str_define)) {
        // define KEY VALUE
        // define KEY
        MyMacro_Buf key;
        if (mymacro_is_success(mymacro_buf_trim_and_chop_by_whitespace(&line, &key))) {
          mymacro_buf_trim_whitespace(&line);
          if (!mymacro_is_success(mymacro__parse_value_and_add(&arena, &macro_list, &key, &line))) {
            // Error
            mymacro_log("mymacro: %.*s:%d: failed to parse value of key `%.*s`\n",
                        (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1,
                        (int)key.len, key.ptr);
            error_count += 1;
          }
        } else {
          // Warning: no key
          mymacro_log("mymacro: %.*s:%d: define has no key\n",
                      (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1);
          error_count += 1;
        }
      } else if (mymacro_buf_eq(&directive, &directive_str_include)) {
        mymacro_buf_trim_whitespace(&line);
        char buf_for_filename[4096];
        if (line.len + 1 > mymacro_array_count(buf_for_filename)) {
          // Error: filename too big
          mymacro_log("mymacro: %.*s:%d: filename to include `%s` is too big\n",
                      (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1,
                      buf_for_filename);
          error_count += 1;
        }
        memcpy(buf_for_filename, line.ptr, line.len);
        buf_for_filename[line.len] = 0;
        switch (mymacro__file_stack_add_file(&file_stack, buf_for_filename)) {
        case MyMacro__FileStackAddFileErr_Success: {} break;
        case MyMacro__FileStackAddFileErr_MaxIncludeDepth: {
          mymacro_log("mymacro: %.*s:%d: reached max include depth\n",
                      (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1);
          error_count += 1;
        } break;
        case MyMacro__FileStackAddFileErr_Filesystem: {
          // Error
          mymacro_log("mymacro: %.*s:%d: failed to open file `%s`\n",
                      (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1,
                      buf_for_filename);
          error_count += 1;
        } break;
        }
      } else {
        // Error: unknown directive
        mymacro_log("mymacro: %.*s:%d: unknown directive `%.*s`\n",
                    (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1,
                    (int)directive.len, directive.ptr);
        error_count += 1;
      }
    } else {
      // Error: no directive on a non empty line
      mymacro_log("mymacro: %.*s:%d: no directive on a non empty line (wtf)\n",
                  (int)file_stack.list->filename.len, file_stack.list->filename.ptr, file_stack.list->line1);
      error_count += 1;
    }
  }
  mymacro__file_stack_deinit(&file_stack);
  if (error_count > 0) {
    mymacro_arena_deinit(&arena);
    return MYMACRO_ERR;
  } else {
    *result = (MyMacro_Result){
      .arena = arena,
      .list = macro_list,
    };
    return MYMACRO_SUCCESS;
  }
}
